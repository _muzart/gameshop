<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'GameShop',
    'language'=>'ru',
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
        'application.vendors.MizzleXML.*',
        'ext.shoppingCart.*',
        'ext.Mailer.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
        'user',
        'admin',
        'shop'=>array(
            'debug'=>false,
        ),
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123456',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),

	),

	// application components
	'components'=>array(
		//for sending emails
		'mailer' => array(
			'class' => 'application.extensions.mailer.EMailer',
			'pathViews' => 'application.views.email',
			'pathLayouts' => 'application.views.email.layouts'
		),
		//user
        'user'=>array(
            // enable cookie-based authentication
            'allowAutoLogin'=>true,
            'loginUrl'=>array('user/login'),
        ),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
        'shoppingCart' =>
            array(
                'class' => 'ext.shoppingCart.EShoppingCart',
            ),
		/*'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=gameshop',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'admin@gameshop.url.ph',
		'domain'=>'http://gameshop.url.ph',
		'xmlFileName'=>'pricelist.xml',
		'smtp' => array(
			"host" => "mx1.hostinger.ru", //smtp сервер
			"debug" => 0, //отображение информации дебаггера (0 - нет вообще)
			"auth" => true, //сервер требует авторизации
			"port" => 2525, //порт (по-умолчанию - 25)
			"username" => "admin@gameshop.url.ph", //имя пользователя на сервере
			"password" => "91309muza", //пароль
			"replyto" => "noreply@gameshop.url.ph", //e-mail ответа
			"fromname" => "Muza Artikov", //имя
			"from" => "noreply@gameshop.url.ph", //от кого
			"charset" => "utf-8", //от кого
		)
	),
);