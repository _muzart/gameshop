<?php
/**
 * файл класса MizzleXML
 * 
 * @author Muzaffar Artikov <artikovmuzaffar@gmail.com>
 * 
 * MizzleXML может быть использовано таким образом
 * <pre>
 *      $mizzle = new MizzleXML();
 *      $mizzle->addCurrency('USD', 32.34);
 *      $mizzle->createPositionByUrl('http://mizzle.ru/product/6/wow-gold-azuregos-alliance', 1.23, 'http://goldshop8934.ru/wow-gold-buy','USD',true);
 *      $mizzle->createPositionByID(6, 19.95, 'http://goldshop8934.ru/wow-gold-buy');
 *      $mizzle->createPositionByHash('wow-gold-azuregos-alliance', 19.95, 'http://goldshop8934.ru/wow-gold-buy');
 *      $mizzle->createPositionByName('1к золота: Азурегос - Альянс', 19.95, 'http://goldshop8934.ru/wow-gold-buy');
 *      $mizzle->prepare();
 *      $mizzle->save('pricelist.xml');
 * </pre>
 * 
 */
class MizzleXML{
    
    /**
     *
     * @var DomDocument 
     */
    private $dom;
    
    /**
     *
     * @var array 
     */
    private $currencies = array('RUR'=>1.0);
    
    /**
     *
     * @var DomElement 
     */
    private $positions;

    /**
     * Constructor
     * Создаем заголовок для xml документа
     * Создаем элемент positions
     */
    public function __construct() {
        $this->dom = new DOMDocument('1.0','utf-8');
        $this->positions = $this->dom->createElement('positions');
    }
    /**
     * Добавляет новую валюту и его текущий курс относительно рубля.
     * @param string $currency код валюты
     * @param float $rate курс относительно рубля
     */       
    public function addCurrency($currency,$rate){
        $this->currencies[$currency] = $rate;
    }
    /**
     * Создание xml элемента позиции 
     * @param string $mizzleUrl url товара на миззл
     * @param float $price цена товара
     * @param string $buyUrl url покупки
     * @param string $currencyId код валюты
     * @param boolean $available доступность товара
     */
    public function createPositionByUrl($mizzleUrl,$price,$buyUrl,$currencyId = 'RUR', $available = true){
        
        $position = $this->dom->createElement('position');
        $product  = $this->dom->createElement('product');
        $url  = $this->dom->createElement('url',$mizzleUrl);
        $product->appendChild($url);
        $_price = $this->dom->createElement('price',$price);
        $currency_id = $this->dom->createElement('currencyid',$currencyId);
        $available = $this->dom->createElement('available',($available)?'true':'false');
        $buy_url = $this->dom->createElement('buy_url',$buyUrl);
        
        $position->appendChild($product);
        $position->appendChild($_price);
        $position->appendChild($currency_id);
        $position->appendChild($available);
        $position->appendChild($buy_url);
        
        $this->positions->appendChild($position);
    }
    /**
     * Создание xml элемента позиции
     * @param string $mizzleId id товара на миззл
     * @param float $price цена товара
     * @param string $buyUrl url покупки
     * @param string $currencyId код валюты
     * @param boolean $available доступность товара
     */
   /* public function createPositionById($mizzleId,$price,$buyUrl,$currencyId = 'RUR', $available = true){
        
        $position = $this->dom->createElement('position');
        $product  = $this->dom->createElement('product');
        $id  = $this->dom->createElement('id',$mizzleId);
        $product->appendChild($id);
        $_price = $this->dom->createElement('price',$price);
        $currency_id = $this->dom->createElement('currencyid',$currencyId);
        $available = $this->dom->createElement('available',($available)?'true':'false');
        $buy_url = $this->dom->createElement('buy_url',$buyUrl);
        
        $position->appendChild($product);
        $position->appendChild($_price);
        $position->appendChild($currency_id);
        $position->appendChild($available);
        $position->appendChild($buy_url);
        
        $this->positions->appendChild($position);
    }
    /**
     * Создание xml элемента позиции
     * @param string $mizzleHash идентификационный хеш товара на миззл
     * @param float $price цена товара
     * @param string $buyUrl url покупки
     * @param string $currencyId код валюты
     * @param boolean $available доступность товара
     */
   /* public function createPositionByHash($mizzleHash,$price,$buyUrl,$currencyId = 'RUR', $available = true){
        
        $position = $this->dom->createElement('position');
        $product  = $this->dom->createElement('product');
        $hash  = $this->dom->createElement('hash',$mizzleHash);
        $product->appendChild($hash);
        $_price = $this->dom->createElement('price',$price);
        $currency_id = $this->dom->createElement('currencyid',$currencyId);
        $available = $this->dom->createElement('available',($available)?'true':'false');
        $buy_url = $this->dom->createElement('buy_url',$buyUrl);
        
        $position->appendChild($product);
        $position->appendChild($_price);
        $position->appendChild($currency_id);
        $position->appendChild($available);
        $position->appendChild($buy_url);
        
        $this->positions->appendChild($position);
    }
    /**
     * Создание xml элемента позиции
     * @param string $mizzleName название товара на миззл
     * @param float $price цена товара
     * @param string $buyUrl url покупки
     * @param string $currencyId код валюты
     * @param boolean $available доступность товара
     */
   /* public function createPositionByName($mizzleName,$price,$buyUrl,$currencyId = 'RUR', $available = true){
        
        $position = $this->dom->createElement('position');
        $product  = $this->dom->createElement('product');
        $name  = $this->dom->createElement('name',$mizzleName);
        $product->appendChild($name);
        $_price = $this->dom->createElement('price',$price);
        $currency_id = $this->dom->createElement('currencyid',$currencyId);
        $available = $this->dom->createElement('available',($available)?'true':'false');
        $buy_url = $this->dom->createElement('buy_url',$buyUrl);
        
        $position->appendChild($product);
        $position->appendChild($_price);
        $position->appendChild($currency_id);
        $position->appendChild($available);
        $position->appendChild($buy_url);
        
        $this->positions->appendChild($position);
    }
    /**
     * Подготовка xml документа в формате миззл
     */
    private function prepare(){
        $mizzle = $this->dom->createElement('mizzle');
        $mizzle->setAttribute('date', date('Y-m-d H:i'));
        $currencies = $this->dom->createElement('currencies');
        foreach($this->currencies as $currency=>$rate){
            $curr = $this->dom->createElement('currency');
            $curr->setAttribute('id', $currency);
            $curr->setAttribute('rate', $rate);
            $currencies->appendChild($curr);
        }
        $mizzle->appendChild($currencies);
        $mizzle->appendChild($this->positions);   
        $this->dom->appendChild($mizzle);
    }
    /**
     * Сохранение xml документа в файл
     * @param string $filename имя файла
     */
    public function save($filename){
		$this->prepare();
        $this->dom->save($filename);
    }
}
?>