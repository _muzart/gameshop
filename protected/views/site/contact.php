<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Contact Us';
$this->breadcrumbs=array(
	'Contact',
);
?>

<h1>Напишите нам</h1>

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="alert alert-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

<p>
	Если у вас есть предложения или вопросы, пожалуйста заполните форму, чтобы связаться с нами. Спасибо.
</p>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<div class="alert alert-info" style="border-left: 3px solid #0099ff">Поля с <span class="required">*</span> обязательны для заполнения. </div>

	<?php echo $form->errorSummary($model); ?>
    <table class="table">
        <tr>
            <td width="60px">
                <?php echo $form->labelEx($model,'name'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'name'); ?>
                <?php echo $form->error($model,'name'); ?>
            </td>
        </tr>

        <tr>
            <td width="60px">
                <?php echo $form->labelEx($model,'email'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'email'); ?>
                <?php echo $form->error($model,'email'); ?>
            </td>
        </tr>

        <tr>
            <td width="60px">
                <?php echo $form->labelEx($model,'subject'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>128)); ?>
                <?php echo $form->error($model,'subject'); ?>
            </td>
        </tr>

        <tr>
            <td width="60px">
                <?php echo $form->labelEx($model,'body'); ?>
            </td>
            <td>
                <?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50)); ?>
                <?php echo $form->error($model,'body'); ?>
            </td>
        </tr>

	<?php if(CCaptcha::checkRequirements()): ?>
        <tr>
            <td width="60px">
            <?php echo $form->labelEx($model,'verifyCode'); ?>
            <?php $this->widget('CCaptcha'); ?>
            </td>
            <td>
            <?php echo $form->textField($model,'verifyCode'); ?>
            <?php echo $form->error($model,'verifyCode'); ?>
            </td>
        </tr>
	<?php endif; ?>

        <tr>
            <td colspan="2">
                <?php echo CHtml::submitButton('Отправить',array('class'=>'btn btn-primary')); ?>
            </td>
        </tr>
    </table>
<?php $this->endWidget(); ?>

</div><!-- form -->

<?php endif; ?>