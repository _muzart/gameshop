<?php
/* @var $this ProductController */
/* @var $data Product */
?>

<div class="col-6 col-sm-6 col-lg-3">
    <div class="panel panel-default" style="height: 220px; overflow:hidden;">
        <div class="panel-body">
            <h5 align="center"> <?php echo CHtml::encode($data->name); ?> </h5>
            <p>
                <?php if(isset($data->image)):?>
                    <img src="<?php echo Yii::app()->baseUrl."/".$data::IMAGE_FOLDER."/".$data->image;?>" height="100" ></img>
                <?php else: ?>
            <p class="product">
                <?php echo $data->name;?>
            </p>
            <?php endif;?>
            </p>
            <p class="intocart">
                <span class="price small"><?php echo $data->price;?> руб.</span>
                <button class="btn btn-default" id="<?php echo $data->id;?>"> <i class="glyphicon glyphicon-shopping-cart"></i> </button>
            </p>
        </div>
    </div>
</div><!--/span-->
