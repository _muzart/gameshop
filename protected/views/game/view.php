<?php
/* @var $this GameController */
/* @var $model Game */

$this->breadcrumbs=array(
	'Games'=>array('index'),
	$model->name,
);


?>

<div class="game">
    <?php echo CHtml::image(Yii::app()->baseUrl.'/'.$model::IMAGE_FOLDER.'/'.$model->image,$model->name,array(
                                                                    'align'=>'left',
                                                                    'hspace'=>'10',
                                                                    'vspace'=>'10',
    ));?>
    <h3>  <?php echo $model->name; ?> </h3>
    <h3> Жанр:  <a href="<?=Yii::app()->createUrl('game/genre',array('id'=>$model->genre_id))?>"> <?php echo $model->genre->name ?></a> </h3>
    <p> <?php echo $model->desc; ?></p>
    <div class="cleared"></div>
    <div class="categories" id="catTabs">
	<h3>Категории</h3>
        <ul class="nav nav-pills">
            <?php $counter=0;
            foreach($model->categories as $cat): $counter++;?>
            <li <?=($counter==1)?'class="active"':''?>><a href="#<?=$cat->short?>" data-toggle="tab"><?=$cat->name?></a></li>
            <?php endforeach;?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <?php $counter=0;
            foreach($model->categories as $cat): $counter++?>
            <div class="tab-pane <?=($counter==1)?"active":""?>" id="<?=$cat->short?>">
				<div class="cleared"></div>
				<div class="btn-group pull-right">
					<button class="btn btn-default" title="С картинками" onclick="js:$('.products.tables').hide();$('.products.pictures').show();"> <i class="glyphicon glyphicon-picture"></i> </button>
					<button class="btn btn-default" title="В виде таблицы" onclick="js:$('.products.pictures').hide();$('.products.tables').show();"> <i class="glyphicon glyphicon-list"></i> </button>
				</div>
				<div class="cleared"></div>
                <div class="products pictures">
				<?php
                    $dataProvider = new CArrayDataProvider($cat->products,array(
						'keyField'=>'id',
						'pagination'=>array(
							'pageSize'=>30,
						),
					));
                    $this->widget('zii.widgets.CListView', array(
                        'dataProvider'=>$dataProvider,
						'ajaxUpdate'=>true,
                        'itemView'=>'_viewproduct',
                    ));
                ?>
				</div>
				<?php  //ar_dump($dataProvider); exit;?>
				<div class="products tables" style="display:none">
					<?php
					$this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'products-grid',
						'ajaxUpdate'=>true,
						'dataProvider'=>$dataProvider,
						'columns'=>array(
							array(
								'name'=>'name',
								'header'=>'Название'
							),
							array(
								'name'=>'price',
								//'htmlOptions'=>array('style'=>'text-align: center'),
								'header'=>'Цена(руб)'
							),
							array(
								'name'=>'id',
								'header'=>''
							),
						),
					));
					?>
				</div>
            </div>
            <?php endforeach;?>
        </div>
    </div>
    <script>
$(document).ready(
	function(){
		$('#catTabs a').click(function (e) {
			e.preventDefault()
			$(this).tab('show')
		});
		$('.grid-view tr > td:nth-child(3)').each(
			function(){
				var id = $(this).html();
				$(this).html('<button class="btn btn-default" id="'+id+'"><i class="glyphicon glyphicon-shopping-cart"></i> В корзину </button>');
			}
		);
		$('.intocart button,td > button').click(
			function(){
				$.ajax({
					'url':'<?=Yii::app()->createUrl("cart/add")?>',
					'data':{'id':this.getAttribute("id")},
					'type':'POST',
					'cache':false,
					'success':function(html){
						$('.cart').html(html);
					}
				})
			}
		);
	}
);

    </script>
</div>
