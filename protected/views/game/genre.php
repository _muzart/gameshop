<?php
/**
 * Created by PhpStorm.
 * User: _muzart
 * Date: 24.11.13
 * Time: 13:41
 */

/* @var $this GameController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Games',
    $genre
);

$dataProvider = new CArrayDataProvider($games);

?>

<h1>Игры <?php echo $genre;?></h1>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'itemView'=>'_view',
)); ?>
