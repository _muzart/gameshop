<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	'Products'=>array('index'),
	$model->name,
);

?>

<h1>Товар <?php echo $model->name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		//'category.name',
		//'game.name',
		//'isAggregator',
		/*array(
			'label'=>'Описание',
			'name'=>'desc'
		),*/

		array(
			'label'=>'Игра',
			'name'=>'category.game.name',
		),
		array(
			'label'=>'Категория',
			'name'=>'category.name',
		),
		array(
			'label'=>'Цена',
			'name'=>'price',
			'value'=>$model->price.' руб.'
		),
		array(
            'type'=>'image',
            'label'=>'',
            'value'=>($model->image) ? Yii::app()->baseUrl."/".$model::IMAGE_FOLDER."/".$model->image : "",
        ),
		//'short',
		//'tags',
		//'price',
		//'count',
		//'available',
		//'watched',
	),
)); ?>
<button id="<?=$model->id?>" class="btn btn-default pull-right"><i class="glyphicon glyphicon-shopping-cart"></i> Добавить в корзину</button>
<script>
	$('.detail-view').addClass('table table-striped').removeClass('detail-view');
	$('button.btn').click(
		function(){
			$.ajax({
				'type':'post',
				'url':'<?=Yii::app()->createUrl("cart/add")?>',
				'data':{'id':this.getAttribute("id")},
				'success':function(html){
					$('.cart').html(html);
				}
			});
		}
	);
</script>