<?php
/* @var $this ProductController */
/* @var $data Product */
?>

<div class="view">


</div>
<div class="col-6 col-sm-6 col-lg-3">
    <div class="panel panel-default" style="height: 220px; overflow:hidden;">
        <div class="panel-body">
            <h5 align="center"> <?php echo CHtml::link(CHtml::encode($data->name),Yii::app()->baseUrl.'/index.php/product/'.$data->id,array('title'=>$data->name)); ?> </h5>
            <p>
                <?php if(isset($data->image)):?>
                <img src="<?php echo Yii::app()->baseUrl."/".$data::IMAGE_FOLDER."/".$data->image;?>" height="100" width="80"></img>
                <?php else: ?>
                <p class="product">
                    <?php echo $data->name;?>
                </p>
                <?php endif;?>
            </p>
            <p>
                <span class="price small"><?php echo $data->price;?> руб.</span>

				<?php echo CHtml::link(' <i class="glyphicon glyphicon-shopping-cart"></i> ','#',array(
					'class'=>'btn btn-default',
					'title'=>'Добавить в корзину',
					'ajax' => array(
						'type'=>'POST', //request type
						'data'=>array('id'=>$data->id),
						'url'=>CController::createUrl('cart/add'), //url to call.
						//Style: CController::createUrl('currentController/methodToCall')
						'update'=>'.cart', //selector to update
						//'data'=>'#Game_id.val()'
						//leave out the data key to pass all form values through
					),
				));?>
            </p>
        </div>
    </div>
</div><!--/span-->
