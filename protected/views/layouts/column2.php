<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<div class="row row-offcanvas row-offcanvas-right">
    <div class="col-xs-12 col-sm-9">
        <p class="pull-right visible-xs">
            <button type="button" class="btn btn-success btn-xs" data-toggle="offcanvas">Toggle nav</button>
        </p>

        <?php echo $content; ?>

    </div><!--/span-->

    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">

        <?php
        $this->beginWidget('zii.widgets.CPortlet', array(
            'title'=>'Operations',
        ));
        $this->widget('zii.widgets.CMenu', array(
            'items'=>$this->menu,
            'htmlOptions'=>array('class'=>'operations'),
        ));
        $this->endWidget();
		?>
		<?php if(Yii::app()->controller->id!='cart'):?>
		<div class="cart well">
			<?php
				$this->widget('CartWidget');
			?>
		</div>
		<?php endif;?>
		<?php
        $this->widget('GenreWidget');
        ?>
		<div class="cleared"></div>
		<div class="akcia">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Акция</h3>
				</div>
				<div class="panel-body">
					<img src="<?=Yii::app()->baseurl?>/images/aksiya.png"/>
				</div>
			</div>
		</div>

    </div><!--/span-->
</div><!--/row-->

<?php $this->endContent(); ?>
