<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="_muzart">

    <title>Gameshop</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl;?>/assets/media/css/bootstrap.css"/>
    <!-- Custom styles for this template -->
    <link href="<?php echo Yii::app()->baseUrl;?>/assets/media/css/offcanvas.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl;?>/assets/media/css/common.css"/>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/assets/media/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/assets/media/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/assets/media/js/mylib.js"></script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
      <script src="<?php echo Yii::app()->baseUrl;?>/assets/media/js/html5shiv.js"></script>
      <script src="<?php echo Yii::app()->baseUrl;?>/assets/media/js/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="container" role="navigation">
    <div class="row">
        <div class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo Yii::app()->baseUrl;?>/index.php">Gameshop</a>
            </div>
            <?php $controller_id = Yii::app()->controller->id;?>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">

                    <li class="<?=($controller_id=='site' && Yii::app()->controller->action->id!='contact')? "active" : ""; ?>"><a href="<?php echo Yii::app()->baseUrl;?>/index.php">Главная</a></li>
                    <li class="<?=Yii::app()->controller->action->id=='contact'? "active" : ""; ?>"><a href="<?php echo Yii::app()->baseUrl;?>/index.php/site/contact">Обратная связь</a></li>
                    <li class="<?=$controller_id=='game'? "active" : ""; ?>"><a href="<?php echo Yii::app()->baseUrl;?>/index.php/game">Игры</a></li>
                    <li class="<?=$controller_id=='product'? "active" : ""; ?>"><a href="<?php echo Yii::app()->baseUrl;?>/index.php/product">Товары</a></li>
					<?php if(!Yii::app()->user->isGuest):?>
					<li class="<?=$controller_id=='profile'? "active" : ""; ?>"><a href="<?php echo Yii::app()->baseUrl;?>/index.php/user/profile">Профиль</a></li>
					<li><a href="<?php echo Yii::app()->baseUrl;?>/index.php/user/logout">Выйти (<?=Yii::app()->user->name?>)</a></li>
					<?php else:?>
					<li class="<?=$controller_id=='login'? "active" : ""; ?>"><a href="<?php echo Yii::app()->baseUrl;?>/index.php/user/login">Войти</a></li>
					<?php endif;?>
					<?php if(Yii::app()->user->getName()=='admin'):?>
					<li><a href="<?php echo Yii::app()->baseUrl;?>/index.php/admin">Admin</a></li>
					<?php endif;?>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container">

    <?php echo $content;?>


    <hr>
    <div class="row">
        <footer>
            <div class="well">
                <p>&copy; Company Nobody knows 2013</p>
                <p>Author: _muzart</p>
            </div>
        </footer>
    </div>

</div><!--/.container-->



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo Yii::app()->baseUrl;?>/assets/media/js/offcanvas.js"></script>
</body>
</html>
