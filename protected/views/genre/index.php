<?php
/* @var $this GenreController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Genres',
);

?>

<h1>Genres</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
