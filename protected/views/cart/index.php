<?php
/* @var $this CartController */

$this->breadcrumbs=array(
	'Cart',
);
$counter = 0;
?>
<h1>Ваша корзина <?=(empty($positions)?" пуста":"")?></h1>
<?php if(!empty($positions)):?>
<table class="table table-striped">
	<tr>
		<th> # </th>
		<th> Товар </th>
		<th> Цена </th>
		<th> Количество </th>
		<th> Общая цена </th>
	</tr>
	<?php foreach($positions as $position):?>
	<tr>
		<td> <?=++$counter?> </td>
		<td> <?=$position->name . " (".$position->category->game->name.")"?></td>
		<td> <?=$position->getPrice()?></td>
		<td> <?=$position->getQuantity()?></td>
		<td> <?=$position->getSumPrice()?></td>
	</tr>
	<?php endforeach;?>
	<tr>
		<th colspan="4">Итого:</th>
		<th><?=$cost?></th>
	</tr>
</table>
<a href="<?=Yii::app()->createUrl('cart/buy')?>" class="btn btn-lg btn-success pull-right">Перейти на покупку &raquo;</a>
<?php endif;?>