<?php echo $input; ?>

<p><select name="<?php echo $select_name; ?>"  id="<?php echo $select_id; ?>" size="<?php echo $size; ?>">
    <?php foreach($list as $key => $val): ?>
	<option <?php foreach($val['attributes'] as $atname => $atvalue) echo "$atname=\"$atvalue\" "; ?> value="<?php echo $key; ?>"><?php echo $val['value']; ?></option>
    <?php endforeach; ?>
</select></p>

<script>
	$(document).ready(function(){
		$('#<?php echo $name; ?>').textList({
			sList: '#<?php echo $select_id; ?>',
			value: '<?php echo $value; ?>'
		});
	});
</script>