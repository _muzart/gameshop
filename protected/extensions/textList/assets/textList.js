(function( $ ){

  var quote = function(str) {
    return (str+'').replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
  };

  $.fn.textList = function( options ) {  
    var settings = $.extend( {
      sList: '',
      sOption: 'option',
      size: 5
    }, options);

    var $list = $(settings.sList),
        listContent = $list.html();

    var init = function(value){
      var $this = $(this);
      $list.val(value);
      var $option = $list.find('option[value='+value+']');
      $this.val($option.text());
    }

    var changed = function(e){
      var $input = $(this);
      var values = $.trim($input.val()).split(' '), pvalues = [];

      if(values.length < 1) return;

      for(var i=0;i<values.length;i++)
        pvalues[i] = quote(values[i]);

      var pattern = new RegExp(pvalues.join('.+?'), 'i');
      $list.html(listContent).find(settings.sOption).filter(function(){
        return !pattern.test($(this).text());
      }).remove();

      var size = $list.find(settings.sOption).length;
      $list.attr('size', Math.min(size, settings.size));
    };

    return this.each(function() {
      var $this = $(this);

      $this.keyup(changed);
      $this.change(changed);

      $list.change(function(){
        var value = $list.val();
        var $option = $list.find('option[value='+value+']');
        $this.val($option.text());
      });

      init.call($this, settings.value);

    });

  };
})( jQuery );