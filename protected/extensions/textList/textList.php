<?php

class TextList extends CInputWidget
{
	public $list = array();
	public $size;
    public $item = array();

	private function generateInputOptions() {
		return array(
				"textlist" => $this->name
			);
	}

    public function run()
    {
        parent::run();

        $baseDir = dirname(__FILE__);
        $assets = Yii::app()->getAssetManager()->publish($baseDir.DIRECTORY_SEPARATOR.'assets');
        $cs = Yii::app()->getClientScript();

        $cs->registerCoreScript('jquery', CClientScript::POS_END);
        $cs->registerScriptFile($assets.'/textList.js', CClientScript::POS_END);
        $html = CHtml::textField($this->name, $this->value, $this->htmlOptions);

        $list = array();
        foreach($this->list as $lk => $lv) {
            $key = $lk;
            $value = $lv;
            $data = $lv;

            $attributes = array();

            if(isset($this->item['id'])) {
                $key = eval('return '.$this->item['id'].';');
            }

            if(isset($this->item['value'])) {
                $value = eval('return '.$this->item['value'].';');
            } else {
                $value = $value->name;
            }

            if(isset($this->item['attributes'])) {
                foreach($this->item['attributes'] as $attname => $attvalue) {
                    $attributes[$attname] = eval('return '.$attvalue.';');
                }
            }

            $list[$key] = array('value'=>$value,
                'attributes' => $attributes);
        }

        $this->list = $list;

        $className = get_class($this->model);
        $name = $this->name;
        CWidget::render('index', array(
        	'input' => $html,
        	'value_id' => $this->model->$name,
        	'value' => $this->value,
        	'name' => $this->name,
        	'list' => $this->list,
        	'size' => $this->size,
        	'select_name' => ($className."[".$this->name."]"),
        	'select_id' => ($className."_".$this->name),
        	));
    }
}

?>