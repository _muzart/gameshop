<?php
return array (
  'template' => 'default',
  'connectionId' => 'db',
  'tablePrefix' => 'gs_',
  'modelPath' => 'application.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
);
