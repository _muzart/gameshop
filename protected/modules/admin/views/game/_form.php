<?php
/* @var $this GameController */
/* @var $model Game */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'game-form',
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <table class="table">
	<tr><td>
		<?php echo $form->labelEx($model,'name'); ?>
        </td><td>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'name'); ?>
	</td></tr>

	<tr><td>
		<?php echo $form->labelEx($model,'short'); ?>
        </td><td>
		<?php echo $form->textField($model,'short',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'short'); ?>
	</td></tr>

	<tr>
        <td>
		<?php echo $form->labelEx($model,'image'); ?>
        </td>
        <td>
		<?php echo $form->fileField($model,'image'); ?>
		<?php echo $form->error($model,'image'); ?>
		<?php
		if(!$model->isNewRecord) {
			echo CHtml::image(Yii::app()->baseUrl.'/images/games/' . $model->image, $model->name, array('height'=>100));
		}
		?>
 	    </td>
    </tr>

	<tr>
        <td>
		<?php echo $form->labelEx($model,'desc'); ?>
        </td>
        <td>
		<?php echo $form->textArea($model,'desc',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'desc'); ?>
	    </td>
    </tr>

	<tr><td>
		<?php echo $form->labelEx($model,'genre_id'); ?>
        </td><td>
		<?php echo $form->dropDownList($model,'genre_id', Genre::getAll()); ?>
		<?php echo $form->error($model,'genre_id'); ?>
	</td></tr>

	<tr><td>
		<?php echo $form->labelEx($model,'tags'); ?>
        </td><td>
		<?php echo $form->textField($model,'tags',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'tags'); ?>
	</td></tr>

	<tr><td>
		<?php echo $form->labelEx($model,'does_sell'); ?>
        </td><td>
		<?php echo $form->dropDownList($model,'does_sell', array('0'=>'Нет','1'=>'Да')); ?>
		<?php echo $form->error($model,'tags'); ?>
	</td></tr>

	<tr><td colspan="2">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</td></tr>
    </table>
<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $("#Game_name").change(function(){
        $("#Game_short").val(toTranslit($('#Game_name').val()));
    });
</script>