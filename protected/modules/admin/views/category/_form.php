<?php
/* @var $this CategoryController */
/* @var $model Category */
/* @var $form CActiveForm */
?>

<script>

</script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'category-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="alert alert-info">Поля отмеченные с <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<table class="table">
        <tr>
            <td>
                <?php echo $form->labelEx($model,'name'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>64)); ?>
                <?php echo $form->error($model,'name'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'parent_id'); ?>
            </td>
            <td>
                <?php echo $form->dropDownList($model,'parent_id',Category::getAll()); ?>
                <?php echo $form->error($model,'parent_id'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'short'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'short',array('size'=>32,'maxlength'=>32)); ?>
                <?php echo $form->error($model,'short'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'tags'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'tags',array('size'=>60,'maxlength'=>256)); ?>
                <?php echo $form->error($model,'tags'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'game_id'); ?>
            </td>
            <td>
                <?php echo $form->dropDownList($model,'game_id',Game::getAll()); ?>
                <?php echo $form->error($model,'game_id'); ?>
            </td>
        </tr>
        <tr>
            <td class="buttons" colspan="2">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
            </td>
        </tr>
	</table>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $("#Category_name").change(function(){
        $("#Category_short").val(toTranslit($('#Category_name').val()));
    });
</script>