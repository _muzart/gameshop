<?php
/* @var $this ProductController */
/* @var $model Product */
/* @var $form CActiveForm */
?>

<div class="form">

<?php 
//print_r($mizzle_products);exit;
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-form',
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
    <table class="table table-striped">
        <tr>
            <td>
                <?php echo $form->labelEx($model,'game_id') ?>
            </td>
            <td>
                <?php echo $form->dropDownList($model,'game_id',Game::getAll(),array(
                    'ajax' => array(
                        'type'=>'POST', //request type
                        //'data'=>array('game_id'=>'js:this.val'),
                        'url'=>CController::createUrl('product/getcats'), //url to call.
                        //Style: CController::createUrl('currentController/methodToCall')
                        'update'=>'#Product_category_id', //selector to update
                        //'data'=>'#Game_id.val()'
                        //leave out the data key to pass all form values through
                    ))); ?>
                <?php echo $form->error($model,'game_id'); ?>
            </td>

        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'category_id'); ?>
            </td>
            <td>
                <?php if(!$model->isNewRecord){
					echo $form->dropDownList($model,'category_id', Category::getAll());
				}
				else
					echo $form->dropDownList($model,'category_id',array()); ?>

                <?php echo $form->error($model,'category_id'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'name'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>256)); ?>
                <?php echo $form->error($model,'name'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'mizzle_product_id'); ?>
            </td>
            <td>
		<?php
			$this->widget('ext.textList.textList', array(
				'model' => $model,
				'name' => 'mizzle_product_id',
				'value' => $model->mizzle_product_id,
				'item' => array(
						'id' => '$data["id"]',
						'value' => '$data["name"]',
					),
				'list' => $mizzle_products,
				'size' => 5,
				'htmlOptions' => array(
						'size'=>60,
						'maxlength'=>128,
					)
			));
		?>
                <?php echo $form->error($model,'mizzle_product_id'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'desc'); ?>
            </td>
            <td>
                <?php echo $form->textArea($model,'desc',array('rows'=>6, 'cols'=>50)); ?>
                <?php echo $form->error($model,'desc'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'image'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'image',array('size'=>64,'maxlength'=>64)); ?>
                <?php echo $form->fileField($model,'image_file'); ?>
                <?php echo $form->error($model,'image'); ?>
                <?php
                    if(!$model->isNewRecord) {
                        echo CHtml::image(Yii::app()->baseUrl.'/images/products/' . $model->image, $model->name, array('height'=>100));
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'short'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'short',array('size'=>32,'maxlength'=>32)); ?>
                <?php echo $form->error($model,'short'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'tags'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'tags',array('size'=>60,'maxlength'=>256)); ?>
                <?php echo $form->error($model,'tags'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'price'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'price'); ?>
                <?php echo $form->error($model,'price'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'count'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'count'); ?>
                <?php echo $form->error($model,'count'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'available'); ?>
            </td>
            <td>
                <?php echo $form->dropDownList($model,'available',array('1'=>'Доступно','0'=>'Недоступно')); ?>
                <?php echo $form->error($model,'available'); ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="row buttons">
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
                </div>
            </td>
        </tr>
    </table>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
    $("#Product_name").change(function(){
        $("#Product_short").val(toTranslit($('#Product_name').val()));
    });
</script>