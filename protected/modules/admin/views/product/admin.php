<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	'Products'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Создать новый товар', 'url'=>array('create')),
	array('label'=>'Создать файл XML', 'url'=>array('feed')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#product-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление товарами</h1>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'product-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'ajaxUpdate'=>false,
	'columns'=>array(
		'id',
		array(
			'name'=>'category.name',
		),
		'category_id',
		'mizzle_product_id',
		'game_id',
		//'isAggregator',
		'name',
		'desc',
		'price',
		/*
		'image',
		'short',
		'tags',
		'count',
		'available',
		'watched',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
