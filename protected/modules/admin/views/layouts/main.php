<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="_muzart">

    <title>Gameshop</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl;?>/assets/media/css/bootstrap.css"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl;?>/assets/media/css/common.css"/>

    <!-- Custom styles for this template -->
    <link href="<?php echo Yii::app()->baseUrl;?>/assets/media/css/offcanvas.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/assets/media/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/assets/media/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/assets/media/js/mylib.js"></script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
      <script src="<?php echo Yii::app()->baseUrl;?>/assets/media/js/html5shiv.js"></script>
      <script src="<?php echo Yii::app()->baseUrl;?>/assets/media/js/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="container" role="navigation">
    <div class="row">
        <div class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo Yii::app()->baseUrl;?>/index.php">Gameshop</a>
            </div>
            <?php $controller_id = Yii::app()->controller->id;?>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="<?=$controller_id=='default'? "active" : ""; ?>"><a href="<?php echo Yii::app()->baseUrl;?>/index.php/admin">Home</a></li>
                    <li class="<?=$controller_id=='game'? "active" : "" ;?>"><a href="<?php echo Yii::app()->baseUrl;?>/index.php/admin/game">Игры</a></li>
                    <li class="<?php echo $controller_id=='genre'? "active" : ""; ?>"><a href="<?php echo Yii::app()->baseUrl;?>/index.php/admin/genre">Жанры</a></li>
                    <li class="<?php echo $controller_id=='category'? "active" : "" ;?>"><a href="<?php echo Yii::app()->baseUrl;?>/index.php/admin/category">Категории</a></li>
                    <li class="<?php echo $controller_id=='product'? "active" : ""; ?>"><a href="<?php echo Yii::app()->baseUrl;?>/index.php/admin/product">Товары</a></li>
                    <li class="<?php echo $controller_id=='user'? "active" : ""; ?>"><a href="<?php echo Yii::app()->baseUrl;?>/index.php/admin/user">Пользователи</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container">

    <?php echo $content;?>


    <hr>
    <div class="row">
        <footer>
            <div class="well">
                <p>&copy; Company Nobody knows 2013</p>
                <p>Author: _muzart </p>
        </footer>
    </div>

</div><!--/.container-->



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo Yii::app()->baseUrl;?>/assets/media/js/offcanvas.js"></script>
</body>
</html>
