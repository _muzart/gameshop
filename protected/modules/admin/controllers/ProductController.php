<?php

class ProductController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $defaultAction = 'admin';
	public $layout='/layouts/column2';
    const IMAGE_FOLDER = '\\images\\products\\';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform all actions
				'actions'=>array('admin','delete','create','update','getcats','index','view','feed'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Product;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Product']))
		{
                    $model->attributes=$_POST['Product'];
                    if($model->save()){
                        if ($model->image_file=CUploadedFile::getInstance($model,'image_file')) {
                            $fileName =  $model->id.'_'.$model->short.'.'.$model->image_file->extensionName;
                            $model->image_file->saveAs(Yii::getPathOfAlias('webroot') . self::IMAGE_FOLDER . $fileName);
                            $model->image = $fileName;
                            $model->save(FALSE);
                }
                $this->redirect('admin/product');
            }
		}
                $command = Yii::app()->db->createCommand("SELECT id, name FROM gs_mizzle_products");
                $mizzle_products = $command->queryAll();
		$this->render('create',array(
                        'mizzle_products'=>$mizzle_products,
			'model'=>$model,
		));
	}

    /**
     * Replaces spaces into " _ "
     */
    public static function renameImageFile($name){
        $symbols = array('?','/','!','"',"'",'+','*','(',')','&','^','%','$','#','@','`','~');
        $name = str_replace(' ','_',$name);
        $name = str_replace($symbols,'',$name);
        return $name;
    }

    /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Product']))
		{
			$model->attributes=$_POST['Product'];
			if($model->save()){
				if ($model->image_file=CUploadedFile::getInstance($model,'image_file')) {
					$fileName =  $model->id.'_'.self::renameImageFile($model->short).'.'.$model->image_file->extensionName;
					$model->image_file->saveAs(Yii::getPathOfAlias('webroot') . self::IMAGE_FOLDER . $fileName);
					$model->image = $fileName;
					$model->save(FALSE);
				}
				$this->redirect(array('admin'));
			}

		}
                
                $command = Yii::app()->db->createCommand("SELECT id, name FROM gs_mizzle_products");
                $mizzle_products = $command->queryAll();
		$this->render('update',array(
			'model'=>$model,
                        'mizzle_products'=>$mizzle_products
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Product');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Product('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Product']))
			$model->attributes=$_GET['Product'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Product the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Product::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Product $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='product-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionGetcats(){
        $data=Category::model()->findAll('game_id=:game_id',
            array(':game_id'=>(int) $_POST["Product"]['game_id']));
        $data=CHtml::listData($data,'id','name');
        foreach($data as $value=>$name)
        {
            echo CHtml::tag('option',
                array('value'=>$value),CHtml::encode($name),true);
        }
    }

	//
	public function actionFeed(){
		$games = Game::model()->findAll();
		$mizzle = new MizzleXML();
		$domain = Yii::app()->params['domain'];
		foreach($games as $game)
			foreach($game->products as $product){
				$mizzle->createPositionByUrl('http://mizzle.ru/product/'.$product->mizzle_product_id.'/'.$product->mizzle->hash, $product->price, $domain . "/game/" . $product->game->id);
			}
		$mizzle->prepare();
		$mizzle->save(Yii::getPathOfAlias('webroot'). "/" .Yii::app()->params["xmlFileName"]);
		$this->render('feed',array(
			'xmlFileName' => Yii::app()->params["xmlFileName"],
		));
	}
}
