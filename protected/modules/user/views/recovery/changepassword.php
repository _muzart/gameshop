<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Change Password");
$this->breadcrumbs=array(
	UserModule::t("Login") => array('/user/login'),
	UserModule::t("Change Password"),
);
?>

<h1><?php echo UserModule::t("Change Password"); ?></h1>


<div class="form">
<?php echo CHtml::beginForm(); ?>
	<table class="table">
	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
	<?php echo CHtml::errorSummary($form); ?>
	
	<tr>
		<td>
		<?php echo CHtml::activeLabelEx($form,'password'); ?>
		</td>
		<td>
		<?php echo CHtml::activePasswordField($form,'password'); ?>
		<p class="hint">
		<?php echo UserModule::t("Minimal password length 4 symbols."); ?>
		</p>
		</td>
	</tr>
	
	<tr>
		<td>
		<?php echo CHtml::activeLabelEx($form,'verifyPassword'); ?>
		</td>
		<td>
		<?php echo CHtml::activePasswordField($form,'verifyPassword'); ?>
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<div class="submit">
				<?php echo CHtml::submitButton(UserModule::t("Save")); ?>
			</div>
		</td>
	</tr>
	</table>
<?php echo CHtml::endForm(); ?>
</div><!-- form -->