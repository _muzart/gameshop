<?php

class CartController extends Controller
{

	public $layout = '//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules(){
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','clear','add','remove'),
				'users'=>array('*'),
			),
			array('allow',
				'actions'=>array('buy'),
				'users'=>array('@')),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionAdd()
	{
		if(Yii::app()->request->IsPostRequest && $_POST["id"]){
			$id = (int) $_POST["id"];
			$product = Product::model()->findByPk($id);
			Yii::app()->shoppingCart->put($product);
		}
		$this->widget('CartWidget');
	}

	public function actionClear()
	{
		if(Yii::app()->request->isPostRequest && $_POST["clear"]){
			Yii::app()->shoppingCart->clear();
		}
		$this->widget('CartWidget');
	}

	public function actionIndex()
	{
		$positions = Yii::app()->shoppingCart->getPositions();
		$cost = Yii::app()->shoppingCart->getCost();
		$this->render('index',array(
			'positions'=>$positions,
			'cost'=>$cost
		));
	}

	public function actionBuy(){
		$this->render('buy');
	}

	public function actionRemove()
	{
		if(Yii::app()->request->IsPostRequest && $_POST["id"]){
			$id = $_POST['id'];
			Yii::app()->shoppingCart->remove($id);
		}

		$this->widget('CartWidget');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}