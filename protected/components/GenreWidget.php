<?php
/**
 * Created by PhpStorm.
 * User: _muzart
 * Date: 22.11.13
 * Time: 0:14
 */

class GenreWidget extends CWidget {

    public function run(){
        $genres = Genre::model()->findAll('id>0');
        $this->render('GenreWidget',array(
            'genres'=>$genres,
        ));
    }
} 