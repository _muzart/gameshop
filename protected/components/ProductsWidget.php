<?php
class ProductsWidget extends CWidget{

    public function run(){
		$sqlCommand = "SELECT p.id, p.name, p.image, p.price FROM gs_product AS p ORDER BY p.id DESC LIMIT 16";
		$products = Yii::app()->db->createCommand($sqlCommand)->queryAll();
        $this->render('ProductsWidget',array(
			'products'=>$products
		));
    }
}