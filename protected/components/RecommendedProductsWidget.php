<?php
/**
 * Created by PhpStorm.
 * User: _muzart
 * Date: 21.11.13
 * Time: 23:58
 */

class RecommendedProductsWidget extends CWidget{

    public function run(){
		$sqlCommand = "SELECT g.id, g.name, g.image, g.desc FROM gs_game AS g ORDER BY g.id DESC LIMIT 5 ";
		$games = Yii::app()->db->createCommand($sqlCommand)->queryAll();
        $this->render('RecommendedProductsWidget',array(
			'games'=>$games,
		));
    }
} 