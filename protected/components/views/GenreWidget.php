<?php if(!empty($genres)):?>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Жанры</h3>
    </div>
    <div class="panel-body">
        <ul class="nav">
            <?php foreach($genres as $genre):?>
            <li class=""><a href="<?php echo Yii::app()->createUrl('game/genre',array('id'=>$genre->id));?>"><?=$genre->name?></a></li>
            <?php endforeach;?>
        </ul>
    </div>
</div>
<?php endif;?>