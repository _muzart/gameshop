<?php
/**
 * Created by PhpStorm.
 * User: _muzart
 * Date: 22.11.13
 * Time: 1:11
 */
//Yii::app()->
?>
    <h3 align="center"><span class="glyphicon glyphicon-shopping-cart"></span> <?php echo CHtml::link('Ваша корзина',Yii::app()->createUrl('/cart'));?></h3>
	<?php if(!empty($positions)):?>
    <table class="table table-striped">
		<tr>
			<th colspan="2"> Товар </th>
			<th> шт. </th>
			<th> Опц. </th>
		</tr>
		<?php foreach($positions as $position):?>
        <tr>
            <td>
				<?=($position->image) ? '<img src="' . Yii::app()->baseUrl.'/images/products/'.$position->image . '" height="30"' : ''?>
				<img src="<?php echo Yii::app()->baseUrl;?>/assets/media/images/battle.jpg" height="30" /></td>
            <td><a href="<?php echo Yii::app()->createUrl('product') . '/'. $position->id?>"><?=$position->name?></a></td>
            <td><?=$position->getQuantity()?></td>
            <td>
				<a href="#" class="trash" id="<?=$position->getId()?>"><i class="glyphicon glyphicon-trash"></i></a>
			</td>
        </tr>
		<?php endforeach;?>
    </table>
    <p align="center">

        <button id="clearCart" class="btn btn-danger" style="margin: 10px">Очистить</button>
        <a href="<?=Yii::app()->createUrl('cart')?>" class="btn btn-success">Оформить</a>
    </p>
	<?php else:?>
		<p>
			Пока пуста...
		</p>
	<?php endif;?>
<script>
	jQuery('#clearCart').click(
		function(){
			if(confirm("Очистить корзину?")){
				jQuery.ajax({
					'type':'POST',
					'data':{'clear':'1'},
					'url':'<?=Yii::app()->createUrl('cart/clear');?>',
					'cache':false,
					'success':function(html){
						jQuery(".cart").html(html)
					}
				});
			}
	});
	jQuery('.trash').click(
		function(){
				$.ajax({
					'type':'POST',
					'data':{'id':this.getAttribute("id")},
					'url':'<?=Yii::app()->createUrl('cart/remove');?>',
					'cache':false,
					'success':function(html){
						jQuery(".cart").html(html)
					}
				});
			return false;
	});


</script>