<div class="row">
    <?php foreach($products as $product):?>
	<div class="col-6 col-sm-6 col-lg-3 wproduct">
        <div class="panel panel-default">
            <div class="panel-body">
                <h4 align="center"><?php echo CHtml::link($product['name'],Yii::app()->createUrl('product') . "/" . $product['id'])?></h4>
                <p><img src="<?php echo Yii::app()->baseUrl . '/images/products/' . $product['image'];?>" height="100" width="80" /></p>
                <p align="center"><span class="price small"> <?php echo $product['price']?> руб. </span><button id="<?php echo $product['id']?>" class="btn btn-default" title="В корзину"> <i class="glyphicon glyphicon-shopping-cart"></i></button></p>
            </div>
        </div>
    </div><!--/span-->
	<?php endforeach?>
<script>
	$('button.btn').click(
		function(){
			$.ajax({
				'type':'post',
				'url':'<?=Yii::app()->createUrl("cart/add")?>',
				'data':{'id':this.getAttribute("id")},
				'success':function(html){
					$('.cart').html(html);
				}
			});
		}
	);
</script>
</div><!--/row-->
<div class="cleared"></div>