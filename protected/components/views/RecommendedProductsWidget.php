
<div id="myCarousel" class="carousel slide">
	<div class="carousel-inner">
		<?php
		$counter = 0;
		foreach($games as $game):
			$counter++; ?>
		<div class="item<?php echo ($counter==1) ? " active" : "";?>">
			<div class="car-in">
				<img src="<?php echo Yii::app()->baseUrl.'/images/games/'.$game['image'];?>" height="" width="150">
				<h1><?php echo CHtml::link(CHtml::encode($game['name']), Yii::app()->createUrl('game') . '/' . $game['id']);?></h1>
				<p> <?php echo (strlen($game['desc'])>300) ? CHtml::encode(mb_substr($game['desc'],0,300,"utf-8")) . "..." : CHtml::encode(mb_substr($game['desc'],0,300,"utf-8"));?></p>
			</div>
		</div>
		<?php endforeach;?>
	</div>
	<a class="carousel-control left" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
	<a class="carousel-control right" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
