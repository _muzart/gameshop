<?php

/**
 * This is the model class for table "gs_category".
 *
 * The followings are the available columns in table 'gs_category':
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 * @property string $short
 * @property string $tags
 * @property integer $game_id
 * @property integer $watched
 *
 * The followings are the available model relations:
 * @property Game $game
 * @property Product[] $products
 */
class Category extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gs_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, short, game_id', 'required'),
			array('parent_id, game_id, watched', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>64),
			array('short', 'length', 'max'=>32),
			array('tags', 'length', 'max'=>256),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, parent_id, short, tags, game_id, watched', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'game' => array(self::BELONGS_TO, 'Game', 'game_id'),
			'products' => array(self::HAS_MANY, 'Product', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'parent_id' => 'Родительская категория',
			'short' => 'Кратко',
			'tags' => 'Теги',
			'game_id' => 'Игра',
			'watched' => 'Присмотрено',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('short',$this->short,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('game_id',$this->game_id);
		$criteria->compare('watched',$this->watched);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public static function getAll(){
        $cats = self::model()->findAll();
        foreach($cats as $cat){
            $cat['name'] .= " (".$cat['game']['name'].")";
        }
        $cats[] = array('0'=>'No');
        $data = CHtml::listData($cats,'id','name');
        return $data;
    }
}