<?php

/**
 * This is the model class for table "gs_game".
 *
 * The followings are the available columns in table 'gs_game':
 * @property integer $id
 * @property string $name
 * @property string $short
 * @property string $image
 * @property integer $rating
 * @property string $desc
 * @property integer $genre_id
 * @property string $tags
 *
 * The followings are the available model relations:
 * @property Category[] $categories
 * @property Genre $genre
 */
class Game extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Game the static model class
	 */
    const IMAGE_FOLDER = "images/games";

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gs_game';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, short, desc, genre_id, tags, does_sell', 'required'),
			array('name, short', 'unique'),
			array('rating, genre_id', 'numerical', 'integerOnly'=>true),
			array('name, image, tags', 'length', 'max'=>256),
            array('image','file', 'types'=>'jpg, gif, png','allowEmpty' => TRUE),
			array('short', 'length', 'max'=>32),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, short, image, rating, desc, genre_id, tags, does_sell', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categories' => array(self::HAS_MANY, 'Category', 'game_id'),
			'products' => array(self::HAS_MANY, 'Product', 'game_id'),
			'genre' => array(self::BELONGS_TO, 'Genre', 'genre_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'short' => 'Кратко',
			'image' => 'Картинка',
			'rating' => 'Рейтинг',
			'desc' => 'Описание',
			'genre_id' => 'Жанр',
			'tags' => 'Теги',
			'does_sell' => 'Валютная игра?',
		);
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('short',$this->short,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('genre_id',$this->genre_id);
		$criteria->compare('tags',$this->tags,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function beforeDelete()
        {
            if(parent::beforeDelete())
            {
                unlink(Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'games'.DIRECTORY_SEPARATOR.$this->image);
                return true;
            }
            return false;
        }

        public static function getAll(){
            $games = self::model()->findAll();
            return CHtml::listData($games,'id','name');
        }


}