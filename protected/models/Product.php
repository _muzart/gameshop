<?php

/**
 * This is the model class for table "gs_product".
 *
 * The followings are the available columns in table 'gs_product':
 * @property integer $id
 * @property integer $category_id
 * @property integer $game_id
 * @property integer $isAggregator
 * @property string $name
 * @property string $desc
 * @property string $image
 * @property string $short
 * @property string $tags
 * @property double $price
 * @property integer $count
 * @property integer $available
 * @property integer $watched
 *
 * The followings are the available model relations:
 * @property Category $category
 */
class Product extends CActiveRecord implements IECartPosition
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
        const IMAGE_FOLDER = "images/products";
        public $image_file;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gs_product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, game_id, name, short, available, mizzle_product_id', 'required'),
            array('name, short, mizzle_product_id', 'unique'),
			array('category_id, game_id, isAggregator, mizzle_product_id, count, available, watched', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('name, tags, image', 'length', 'max'=>256),
            array('image_file','file','types'=>'jpg, gif, png','allowEmpty' => TRUE),
			array('short', 'length', 'max'=>32),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, mizzle_product_id, category_id, game_id, isAggregator, name, desc, short, tags, price, count, available, watched', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'game' => array(self::BELONGS_TO, 'Game', 'game_id'),
			'mizzle' => array(self::BELONGS_TO, 'MizzleProduct', 'mizzle_product_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Категория',
			'game_id' => 'Игра',
			'isAggregator' => 'Аггрегатор',
			'name' => 'Название',
			'desc' => 'Описание',
			'image' => 'Картинка',
			'short' => 'кратко',
			'tags' => 'Теги',
			'price' => 'Цена',
			'count' => 'Количество',
			'available' => 'Доступность',
			'watched' => 'Просмотры',
			'mizzle_product_id' => 'Соответствующий товар в Mizzle',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('game_id',$this->game_id);
		$criteria->compare('isAggregator',$this->isAggregator);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('short',$this->short,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('count',$this->count);
		$criteria->compare('available',$this->available);
		$criteria->compare('watched',$this->watched);
		$criteria->compare('mizzle_product_id',$this->mizzle_product_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function beforeDelete()
        {
            if(parent::beforeDelete())
            {
                unlink(Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'products'.DIRECTORY_SEPARATOR.$this->image);
                return true;
            }
            return false;
        }
        
        function getId(){
            return 'Product'.$this->id;
        }

        function getPrice(){
            return $this->price;
        }
}