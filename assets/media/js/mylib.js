/**
 * Created by _muzart on 05.12.13.
 */
function toTranslit(text) {
    return text.replace(/([а-яёА-ЯЁ])|([\s_-])|([^a-zA-Z\d])/gi,
        function (all, ch, space, words, i) {
            if (space || words) {
                return space ? '-' : '';
            }
            var code = ch.charCodeAt(0),
                index = code == 1025 || code == 1105 ? 0 :
                    code > 1071 ? code - 1071 : code - 1039,
                t = ['yo', 'a', 'b', 'v', 'g', 'd', 'e', 'zh',
                    'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p',
                    'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh',
                    'shch', '', 'y', '', 'e', 'yu', 'ya'
                ];
            return t[index];
        });
}

$(document).ready(
	function(){
		$('.operations').addClass('nav nav-pills nav-stacked');
		$('.grid-view table').addClass('table table-striped').removeClass('items');
		$('div.summary').css({'margin':'10px'}).html("");
		//$('div.pager').css({'width':'100%','display':'inline-block','height':'50px'});
		$('ul.yiiPager').addClass('pagination').removeClass('yiiPager');
		$('.errorSummary').addClass('alert alert-danger').removeClass('errorSummary');
		$('ul.pagination li.selected').addClass('active').removeClass('selected');
		var pagination = $('ul.pagination');
		$('.pager').html(pagination);
	}

);
